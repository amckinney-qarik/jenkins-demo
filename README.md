# Jenkins demo

An example values file for the community Jenkins helm chart, to be deployed on
GKE.

**TL;DR** 

1. clone out this repo
2. `helm repo add jenkinsci https://charts.jenkins.io`
3. `helm repo update`
4. `helm install jenkins jenkinsci/jenkins -f jenkins-values.yaml -n ci --create-namespace`

#### Intro

The repo contains a jenkins-values.yaml file, which can be used to pass values to
a helm chart install.

The values try to show off some integration with GCP, so some annotations are
being used in the `ingress` definition, specifically:

 * [external-dns](https://github.com/kubernetes-sigs/external-dns) - a tool to automatically update DNS based on kubernetes manifests.
 * Google Cloud [Managed TLS certs](https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs)

To leverage these features, so additional work is required (e.g deploying the external-dns
application, configuring a CloudDNS zone, delegating a zone for CloudDNS etc).

Some example yaml has been provided that shows how trivial these deployments can be.

#### Cluster setup (optional)

1. Prepare environment

```
export PROJECT_ID=acmck-lab
export GCP_ZONE=us-west1-b
gcloud config set project $PROJECT_ID
gcloud config set compute/zone $GCP_ZONE
```

2. Create GKE called jenkins.

```
gcloud container clusters create "jenkins" \
    --num-nodes 3 \
    --cluster-version 1.17 \
    --scopes "https://www.googleapis.com/auth/ndev.clouddns.readwrite"
```

**N.B.** We use scopes here, to allow the GKE to use other GCP services. The recommended
way to handle this access is now by using [Workload Identity](https://cloud.google.com/blog/products/containers-kubernetes/introducing-workload-identity-better-authentication-for-your-gke-applications)

3. Obtain a kubeconfig for jenkins cluster.

```
gcloud container clusters get-credentials jenkins --zone $GCP_REGION
```

4. Ensure connectivity to cluster

```
kubectl version
```


#### Helm setup

1. Add the Jenkins chart repository, Prometehsus Operator if you want to use it,
and finally update your helm repos.

```
helm repo add jenkinsci https://charts.jenkins.io
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

2. Search for the charts we intend to use.

```
helm repo search prometheus-operator
helm search repo jenkinsci
```

You should see two hits like:

```
✗ helm search repo prometheus-operator

NAME                       	CHART VERSION	APP VERSION	DESCRIPTION
bitnami/prometheus-operator	0.31.0       	0.41.0     	The Prometheus Operator for Kubernetes provides...
stable/prometheus-operator 	9.3.2        	0.38.1     	DEPRECATED Provides easy monitoring definitions...

✗ helm search repo jenkinsci

NAME             	CHART VERSION	APP VERSION	DESCRIPTION
jenkinsci/jenkins	2.7.1        	lts        	Open source continuous integration server. It s...
```

#### Deploy charts

If you'd like to use the prometheus operator, which would allow for some grafana [dashboards](https://grafana.com/grafana/dashboards/9964), please uncomment the prometehesu block from Jenkins values, and install the operator.

**Note:** The helm install commands assume the repos have been named as above. If
repos were renamed, update in the install lines below.

1. Install the prom-operator chart into the monitoring namespace, using default 
values. It's a new namespace/cluster, so lets ask helm to create the namesapce 
for us.

```
helm install prom-oper bitnami/kube-prometheus -n monitoring --create-namespace
```

2. Install the jenkins chart, using the values from this repo.

```
helm install jenkins jenkinsci/jenkins -f jenkins-values.yaml -n ci --create-namespace
```


